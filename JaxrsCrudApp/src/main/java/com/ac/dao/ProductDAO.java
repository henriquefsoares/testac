package com.ac.dao;
 
import java.util.List;
 
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
 
public class ProductDAO {
    
	public void addProduct(Product bean){
        Session session = SessionUtil.getSession();        
        Transaction tx = session.beginTransaction();
        addProduct(session,bean);        
        tx.commit();
        session.close();
    }
    
    private void addProduct(Session session, Product bean){
        Product product = new Product();
        
        product.setName(bean.getName());
        product.setDescription(bean.getDescription());
        product.setParentId(bean.getParentId());
        
        session.save(product);
    }
    
    public List<Product> getProducts(){
        Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Product");
        List<Product> products = query.list();
        session.close();
        return products;
    }
    
    //TODO
//    public List<Product> getProductsWithRelationship(){
//        Session session = SessionUtil.getSession();    
//        Query query = session.createQuery("from Product");
//        List<Product> products = query.list();
//        session.close();
//        Session session2 = SessionUtil.getSession();
//        Query query2 = session2.createQuery("from Image");
//        List<Image> images = query2.list();
//        session2.close();
//        for(Product p : products){
//        	for(Product p2 : products){
//        		if(p.getId()==p2.getParentId()) p.getChildProducts().add(p2);
//        	}
//        	for(Image i : images){
//        		if(p.getId()==i.getProductId()) p.getImages().add(i);
//        	}
//        }
//        
//        return products;
//    }
//    
////TODO
//  	public Product getProductWithRelationship(int id) {
//  		Session session = SessionUtil.getSession();    
//        Query query = session.createQuery("from Product where id = :id");
//        query.setInteger("id",id);
//        Product product = (Product) query.uniqueResult();
//        session.close();
//        Session session2 = SessionUtil.getSession();
//        Query query2 = session2.createQuery("from Image where id = :id");
//        query2.setInteger("id",id);
//        List<Image> images = query2.list();
//        session2.close();
//        Session session3 = SessionUtil.getSession();    
//        Query query3 = session.createQuery("from Product");
//        List<Product> products = query3.list();
//        
//        for(Product p : products){
//    		if(product.getId()==p.getParentId()) product.getChildProducts().add(p);
//    	}
//    	for(Image i : images){
//    		if(product.getId()==i.getProductId()) product.getImages().add(i);
//    	}
//    	
//        return product;
//  	}
 
	public int deleteProduct(int id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        String hql = "delete from Product where id = :id";
        Query query = session.createQuery(hql);
        query.setInteger("id",id);
        int rowCount = query.executeUpdate();
        System.out.println("Rows affected: " + rowCount);
        tx.commit();
        session.close();
        return rowCount;
    }
    
    public int updateProduct(int id, Product prod){
         if(id <=0)  
               return 0;  
         Session session = SessionUtil.getSession();
         Transaction tx = session.beginTransaction();
         String hql = "update Product set name = :name, description=:description, parent_product_id =:parent_product_id where id = :id";
         Query query = session.createQuery(hql);
         query.setInteger("id",id);
         query.setString("name",prod.getName());
         query.setString("description",prod.getDescription());
         query.setInteger("parent_product_id",prod.getParentId());
         int rowCount = query.executeUpdate();
         System.out.println("Rows affected: " + rowCount);
         tx.commit();
         session.close();
         return rowCount;
    }

	public Product getProduct(int id) {
		Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Product where id = :id");
        query.setInteger("id",id);
        Product product = (Product) query.uniqueResult();
        session.close();
        return product;
	}

	public List<Product> getChildProducts(int parent_product_id) {
		Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Product where parent_product_id = :parent_product_id");
        query.setInteger("parent_product_id",parent_product_id);
        List<Product> products = query.list();
        session.close();
        return products;
	}

	public void deleteAllProducts() {
		Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        String hql = "delete from Product";
        Query query = session.createQuery(hql);
        query.executeUpdate();
        tx.commit();
        session.close();
	}
}