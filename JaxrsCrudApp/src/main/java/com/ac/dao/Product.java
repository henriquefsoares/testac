
 
package com.ac.dao;
 
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
@Entity
@Table(name="PRODUCT")
public class Product {
 
    @Id
    @Column(name="product_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="name")
    private String name;
    
    @Column(name="description")
    private String description;
    
    @Column(name="parent_product_id")
    private int parent_product_id;
    
//    @OneToOne
//    private Product parent;
//    
//    @OneToMany
//    @JoinColumn(name="parent_product_id")
//    private List<Product> childProducts = new ArrayList<>();
//    
//    @OneToMany
//    @JoinColumn(name="image_id")
//    private List<Image> images;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public int getParentId() {
        return parent_product_id;
    }
    
    public void setParentId(int parent_product_id) {
        this.parent_product_id = parent_product_id;
    }
    
//    public void setChildProducts(List<Product> childProducts){
//    	this.childProducts = childProducts;
//    }
//    
//    public List<Product> getChildProducts(){
//    	return childProducts;
//    }
//    
//    public void setImages(List<Image> images){
//    	this.images = images;
//    }
//    
//    public List<Image> getImages(){
//    	return images;
//    }
}