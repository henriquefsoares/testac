package com.ac.dao;
 
import java.util.List;
 
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
 
public class ImageDAO {
    
    public void addImage(Image bean){
        Session session = SessionUtil.getSession();        
        Transaction tx = session.beginTransaction();
        addImage(session,bean);        
        tx.commit();
        session.close();
    }
    
    private void addImage(Session session, Image bean){
        Image Image = new Image();
        
        Image.setType(bean.getType());
        Image.setProductId(bean.getProductId());
        
        session.save(Image);
    }
    
    public List<Image> getImages(){
        Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Image");
        List<Image> Images = query.list();
        session.close();
        return Images;
    }
    
    public int deleteImage(int id) {
        Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        String hql = "delete from Image where id = :id";
        Query query = session.createQuery(hql);
        query.setInteger("id",id);
        int rowCount = query.executeUpdate();
        System.out.println("Rows affected: " + rowCount);
        tx.commit();
        session.close();
        return rowCount;
    }
    
    public int updateImage(int id, Image prod){
         if(id <=0)  
               return 0;  
         Session session = SessionUtil.getSession();
         Transaction tx = session.beginTransaction();
         String hql = "update Image set type = :type, product_id=:product_id where id = :id";
         Query query = session.createQuery(hql);
         query.setInteger("id",id);
         query.setString("type",prod.getType());
         query.setInteger("product_id",prod.getProductId());
         int rowCount = query.executeUpdate();
         System.out.println("Rows affected: " + rowCount);
         tx.commit();
         session.close();
         return rowCount;
    }

	public List getImagesFromProduct(int product_id) {
		Session session = SessionUtil.getSession();    
        Query query = session.createQuery("from Image where product_id = :product_id");
        query.setInteger("product_id",product_id);
		List<Image> Images = query.list();
        session.close();
        return Images;
	}

	public void deleteAllImages() {
		Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
        String hql = "delete from Image";
        Query query = session.createQuery(hql);
        query.executeUpdate();
        tx.commit();
        session.close();
	}
}