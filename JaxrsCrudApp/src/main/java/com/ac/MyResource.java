package com.ac;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ac.dao.Image;
import com.ac.dao.ImageDAO;
import com.ac.dao.Product;
import com.ac.dao.ProductDAO;


@Path("myresource")
public class MyResource {
	
	@GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello World !!";
    }

	@POST
    @Path("product/create")
    @Consumes("application/json")
    public Response addProduct(Product prod){
        prod.setName(prod.getName());
        prod.setDescription(prod.getDescription());
        prod.setParentId(prod.getParentId());
        ProductDAO dao = new ProductDAO();
        dao.addProduct(prod);
        return Response.ok().build();
    }
	
	@GET
	@Path("product")
    @Produces("application/json")
    public List<Product> getProduct() {
        ProductDAO dao = new ProductDAO();
        List products = dao.getProducts();
        return products;
    }
	
	@GET
	@Path("product/{id}")
    @Produces("application/json")
    public Product getProductById(@PathParam("id") int id) {
        ProductDAO dao = new ProductDAO();
        Product product = dao.getProduct(id);
        return product;
    }
	
	@GET
	@Path("product/justchilds/{id}")
    @Produces("application/json")
    public List<Product> getChildProducts(@PathParam("id") int id) {
        ProductDAO dao = new ProductDAO();
        List products = dao.getChildProducts(id);
        return products;
    }
	
//	@GET
//	@Path("productWithRelationship")
//    @Produces("application/json")
//    public List<Product> getProductWithRelationship() {
//        ProductDAO dao = new ProductDAO();
//        List products = dao.getProductsWithRelationship();
//        return products;
//    }
//	
//	@GET
//	@Path("productWithRelationship/{id}")
//    @Produces("application/json")
//    public Product getProductWithRelationshipById(@PathParam("id") int id) {
//        ProductDAO dao = new ProductDAO();
//        Product product = dao.getProductWithRelationship(id);
//        return product;
//    }

	@PUT
    @Path("product/update/{id}")
    @Consumes("application/json")
    public Response updateProduct(@PathParam("id") int id, Product prod){
        ProductDAO dao = new ProductDAO();
        int count = dao.updateProduct(id, prod);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
	}
	
	@DELETE
    @Path("product/delete/{id}")
    @Consumes("application/json")
    public Response deleteProduct(@PathParam("id") int id){
        ProductDAO dao = new ProductDAO();
        int count = dao.deleteProduct(id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	@GET
    @Path("image")
    @Produces("application/json")
    public List<Image> getImages() {
        ImageDAO dao = new ImageDAO();
        List images = dao.getImages();
        return images;
    }
	
	@GET
    @Path("product/justimages/{id}")
    @Produces("application/json")
    public List<Image> getImagesForProduct(@PathParam("id") int id) {
        ImageDAO dao = new ImageDAO();
        List images = dao.getImagesFromProduct(id);
        return images;
    }
	
	@POST
    @Path("image/create")
    @Consumes("application/json")
    public Response addImage(Image image){
        image.setType(image.getType());
        image.setProductId(image.getProductId());
        ImageDAO dao = new ImageDAO();
        dao.addImage(image);
        return Response.ok().build();
    }
	
	@PUT
    @Path("image/update/{id}")
    @Consumes("application/json")
    public Response updateImage(@PathParam("id") int id, Image image){
    	ImageDAO dao = new ImageDAO();
        int count = dao.updateImage(id, image);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	@DELETE
    @Path("image/delete/{id}")
    @Consumes("application/json")
    public Response deleteImage(@PathParam("id") int id){
    	ImageDAO dao = new ImageDAO();
        int count = dao.deleteImage(id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
	
	@DELETE
    @Path("deleteall")
    @Consumes("application/json")
    public void deleteAll(){
		ImageDAO imageDao = new ImageDAO();
		imageDao.deleteAllImages();
		ProductDAO productDao = new ProductDAO();
		productDao.deleteAllProducts();
    }
}
