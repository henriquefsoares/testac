package com.ac.client;

import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Tests {

	@Before
	public void setup()
	{
		Client c  = Client.create();
		WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/deleteall");
	}
	
	@Test
	public void createProduct_shouldReturnSucess()
    {
		String input = "{\"name\":\"house\", \"description\": \"big\"}";
		assertTrue(checkOk(addProduct(input)));
		deleteProduct(1);
    }
	
	@Test
	public void deleteProduct_shouldReturnSucess()
    {
		addProduct("{\"name\":\"house\", \"description\": \"big\"}");
		assertTrue(checkOk(deleteProduct(1)));
    }
	
	@Test
	public void getProduct_shouldReturnSucess()
    {
		addProduct("{\"name\":\"house\", \"description\": \"big\"}");
		assertTrue(checkOk(getProductStatus()));
		deleteProduct(1);
    }
	
	@Test
	public void getProductById_shouldReturnSucess()
    {
		addProduct("{\"name\":\"house\", \"description\": \"big\"}");
		assertTrue(checkIdEqualsOne(getProductId(1)));
		deleteProduct(1);
    }
	
	@Test
	public void createImage_shouldReturnSucess()
    {
		String input = "{\"type\":\"image\", \"product_id\":1}";
		assertTrue(checkOk(addImage(input)));
		deleteImage(1);
    }
	
	@Test
	public void deleteImage_shouldReturnSucess()
    {
		addImage("{\"type\":\"image\", \"product_id\":1}");
		assertTrue(checkOk(deleteImage(1)));
    }
	
	@Test
	public void getImages_shouldReturnSucess()
    {
		addImage("{\"type\":\"image\", \"product_id\":1}");
		assertTrue(checkOk(getImages()));
    }
	
	@Test
	public void getImagesForProduct_shouldReturnSucess()
    {
		addProduct("{\"name\":\"house\", \"description\": \"big\"}");
		addImage("{\"type\":\"image\", \"product_id\":1}");
		assertTrue(checkOk(getImagesForProduct(1)));
		deleteProduct(1);
		deleteImage(1);
    }
	
	@Test
	public void getChildProducts_shouldReturnSucess()
	{
		addProduct("{\"name\":\"house\", \"description\": \"big\"}");
		addProduct("{\"name\":\"house\", \"description\": \"big\", \"parent_product_id\":1}");
		assertTrue(checkOk(getChildProducts(1)));
		deleteProduct(1);
		deleteProduct(2);
	}
	
	//Utils
    private static ClientResponse getImagesForProduct(int id) {
    	Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/justimages" + id);
        return resource.get(ClientResponse.class);
	}

	private static ClientResponse getChildProducts(int id) {
		Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/justchilds" + id);
        return resource.get(ClientResponse.class);
	}

	private static ClientResponse addProduct(String input){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/create");
        return resource.type("application/json").post(ClientResponse.class, input);
    }

	private static boolean checkOk(ClientResponse response) {
		return response.getStatus() != 200;
	}
    
    private static ClientResponse getProductStatus(){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product");
        
        return resource.get(ClientResponse.class);
    }
    
    private int getProductId(int id) {
    	Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/" + id);
        return resource.get(JSONObject.class).getInt("id");
	}
    
    private boolean checkIdEqualsOne(int productId) {
		return productId == 1;
	}

    private static void getProductWithRelationship(){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/productWithRelationship");
        
        String response = resource.get(String.class);
        System.out.println(response);
    }
    
    private static void getProductById(int id){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/" + id);
        
        String response = resource.get(String.class);
        System.out.println(response);
    }
    
    private static void getProductWithRelationshipById(int id){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/productWithRelationship/" + id);
        
        String response = resource.get(String.class);
    }
    
    public static ClientResponse deleteProduct(int id){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/product/delete/" + id);
        return resource.type("application/json").delete(ClientResponse.class);
    }
    
    private static ClientResponse addImage(String input){
        Client c  = Client.create();
        WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/image/create");
        return resource.type("application/json").post(ClientResponse.class, input);
    }

	private static ClientResponse getImages(){
		Client c  = Client.create();
		WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/image");
		return resource.get(ClientResponse.class);
	}
	
	public static ClientResponse deleteImage(int id){
		Client c  = Client.create();
		WebResource resource = c.resource("http://localhost:8080/JaxrsCrudApp/webapi/myresource/image/delete/" + id);
		return resource.type("application/json").delete(ClientResponse.class);
	}
}
